var express = require('express'),
    app = express();

//Express 3
//app.configure(function() {
//    app.use(express.static(__dirname, '/'));
//});

//Express 4
app.use(express.static(__dirname + '/'));

app.get('/customers/:id', function(req, res) {
    var customerId = parseInt(req.params.id);
    var data = {};
    for (var i=0,len=customers.length;i<len;i++) {
        if (customers[i].id === customerId) {
           data = customers[i];
           break;
        }
    }  
    res.json(data);
});

app.get('/customers', function(req, res) {
    res.json(customers);
    //res.json(500, { error: 'An error has occurred!' });
});

app.get('/orders', function(req, res) {
    var orders = [];
    for (var i=0,len=customers.length;i<len;i++) {
        if (customers[i].orders) {
            for (var j=0,ordersLen=customers[i].orders.length;j<ordersLen;j++) {
                orders.push(customers[i].orders[j]);   
            }
        }
    }  
    res.json(orders);
});

app.delete('/customers/:id', function(req, res) {
    var customerId = parseInt(req.params.id);
    var data = { status: true };
    for (var i=0,len=customers.length;i<len;i++) {
        if (customers[i].id === customerId) {
           customers.splice(i,1);
           data = { status: true };
           break;
        }
    }  
    res.json(data);
});

app.listen(8080);

console.log('Express listening on port 8080');

        var customers = [
            {
                id: 1, 
                joined: '2020-12-03',
                name:'Bryan',
                city:'Sheboygan',
                orderTotal: 10.05,
                orders: [
                    {
                        id: 1,
                        product: 'Shampoo',
                        total: 10.05
                    }
                ]
            }, 
            {
                id: 2, 
                joined: '1995-02-06',
                name:'Dennis',
                city:'Chicago',
                orderTotal: 30.00,
                orders: [
                    {
                        id: 2,
                        product: 'Wine',
                        total: 10.00
                    },
                    {
                        id: 3,
                        product: 'Whiskey',
                        total: 20.00
                    }
                ]
            },
            {
                id: 3, 
                joined: '2010-04-21',
                name:'Brian',
                city:'Minneapolis',
                orderTotal:50.00,
                orders: [
                    {
                        id: 4,
                        product: 'Xbox Controller',
                        total: 50.00
                    }
                ]
            }, 
            {
                id: 4, 
                joined: '1999-08-27',
                name:'Karen',
                city:'Pittsburgh',
                orderTotal:900.00,
                orders: [
                    {
                        id: 5,
                        product: 'iPhone X',
                        total: 900.00
                    }
                ]
            }
        ];